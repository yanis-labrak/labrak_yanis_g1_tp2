package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {

    WineDbHelper db;

    Wine wine;
    Wine wineReceived;

    EditText editTitle;
    EditText editWineRegion;
    EditText editLoc;
    EditText editClimate;
    EditText editPlantedArea;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();

        this.linkFields();

//        Wine wine = intent.getStringExtra("wine");
        this.wineReceived = (Wine) getIntent().getParcelableExtra("Wine");

        if (wineReceived != null) {
            Log.d("wineReceived","not null");
            this.wine = wineReceived;
            this.initFields();
        } else {
            Log.d("wineReceived","null");
            this.wine = new Wine();
        }

//        wine = new Wine();
        db = new WineDbHelper(this);
    }

    private void linkFields() {
        editTitle = findViewById(R.id.editTitle);
        editWineRegion = findViewById(R.id.editWineRegion);
        editLoc = findViewById(R.id.editLoc);
        editClimate = findViewById(R.id.editClimate);
        editPlantedArea = findViewById(R.id.editPlantedArea);
        button = findViewById(R.id.button);
    }

    private void initFields() {
        editTitle.setText(this.wine.getTitle());
        editWineRegion.setText(this.wine.getRegion());
        editLoc.setText(this.wine.getLocalization());
        editClimate.setText(this.wine.getClimate());
        editPlantedArea.setText(this.wine.getPlantedArea());
    }

    public void save(View view) {

        String editTitleText = editTitle.getText().toString();
        String editWineRegionText = editWineRegion.getText().toString();
        String editLocText = editLoc.getText().toString();
        String editClimateText = editClimate.getText().toString();
        String editPlantedAreaText = editPlantedArea.getText().toString();

        if (editTitleText == null || editTitleText.equals("")) {
            WineActivity.displayDialog("Sauvegarde impossible", "Le nom du vin doit être non vide.", WineActivity.this);
        } else {

            this.wine.setTitle(editTitleText);
            this.wine.setRegion(editWineRegionText);
            this.wine.setLocalization(editLocText);
            this.wine.setClimate(editClimateText);
            this.wine.setPlantedArea(editPlantedAreaText);

            boolean inserted;

            if (this.wineReceived != null) {
                inserted = db.updateWine(this.wine) > 0;
            } else {
                inserted = db.addWine(this.wine);
            }

            if (inserted == true) {

                Toast.makeText(WineActivity.this, "Sauvegarder !", Toast.LENGTH_LONG).show();

                Intent myIntent = new Intent(WineActivity.this, MainActivity.class);
                WineActivity.this.startActivity(myIntent);
            } else {
                WineActivity.displayDialog("Ajout impossible", "Un vin portant le même nom existe déjà dans la base de données", WineActivity.this);
            }
        }
    }

    public static void displayDialog(String title, String message, Context context){

        final AlertDialog alert = new AlertDialog.Builder(context).create();
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        }, 4000);
    }
}
